
``real-time-settings``
======================

``./real-time-settings --help``::

  usage: real-time-settings [-h] [-d] [-v] [-s] [-l] [-i [IGNORE ...]]
                            [{on,off}]
  
  This script configures Linux user space for real-time applications (such as
  audio applications). It also stores previous configurations, so changes can be
  rolled back. Tested for recent Debian systems.
  
  positional arguments:
    {on,off}              Activate or rt_settings_off. (default: None)
  
  options:
    -h, --help            show this help message and exit
    -d, --debug           turn on debug messages (default: False)
    -v, --verbose         turn on verbose messages (default: False)
    -s, --simulate        simulate and print what would be executed (default:
                          False)
    -l, --list            list available actions and exit (default: False)
    -i [IGNORE ...], --ignore [IGNORE ...]
                          list of actions to ignore (default: [])

``./real-time-settings --list``::

  Anacron – de/activates anacron task scheduler
  CheckForRealTimeKernel – checks whether system uses a real-time kernel, warns if not
  CpuFrequencyLimits – controls processors' frequency limits
  CpuGovernor – controls the processors' frequency scaling strategy
  Cron – de/activates cron task scheduler
  PlatformProfile – controls hardware power profile
  Tlp – de/activates TLP power save features
  TuneD – tries to instruct tuned to also apply latency-optimized settings
  TurboBoost – controls processors' (Intel) TurboBoost feature

