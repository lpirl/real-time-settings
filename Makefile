MAKEFLAGS += -rs

README.rst: real-time-settings Makefile
	echo > $@
	echo '``$<``' >> $@
	(echo -n ==; echo -n $< | sed 's/./=/g'; echo ==)  >> $@
	echo >> $@
	echo '``./$< --help``::' >> $@
	echo >> $@
	./$< --help 2>&1 | sed 's/^/  /' >> $@
	echo >> $@
	echo '``./$< --list``::' >> $@
	echo >> $@
	./$< --list 2>&1 | sed 's/^/  /' >> $@
	echo >> $@
